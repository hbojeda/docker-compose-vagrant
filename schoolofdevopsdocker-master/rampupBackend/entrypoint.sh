#!/bin/sh

./wait-for-it.sh mysql-srv:3306 --strict --timeout=10000 -- node seeds.js

exec "$@"
