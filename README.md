**Practico e Virtualization & Containerization y Docker**

- **Folder Vagrant-Compose:** contain Vagrant file with configuration of de vagrants machine and the docker-compose-swarm.yaml that deploy

- **Folder schoolofdevopsdocker-master:** contain de clone of the repo SchoolOfDevopsDocker, plus the Dockerfile of each machine and Docker-compose.yaml configuration

